package nus.iss.spring.security.controller;

import nus.iss.spring.security.domain.Program;
import nus.iss.spring.security.service.ProgramsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/secured")
public class ProgramController {

    @Autowired
    private ProgramsService programsService;

    @GetMapping(path = "/programs")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Iterable<Program> getPrograms(HttpServletRequest request) {
        return programsService.findAllPrograms();
    }

    @PostMapping(path = "/programs")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Program createProgram(HttpServletRequest request, @Valid @RequestBody Program program) {
        return programsService.createProgram(program);
    }

    @PutMapping(path = "/programs")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Program editProgram(HttpServletRequest request, @RequestBody Program program) {
        return programsService.editProgram(program);
    }

    @DeleteMapping(path = "/programs/{id}")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Program deleteProgram(HttpServletRequest request, @PathVariable(name = "id") Long id) {
        return programsService.deleteProgram(id);
    }

}
