package nus.iss.spring.security.controller;

import nus.iss.spring.security.domain.User;
import nus.iss.spring.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/secured")
public class UserController {
    @Autowired
    private UserService userService;


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('STANDARD_USER')  or hasAuthority('MANAGER_USER') ")
    public List<User> getUsers(HttpServletRequest request) {
        return userService.findAllUsers();
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public User createUser(HttpServletRequest request, @Valid @RequestBody User user) {

        return userService.createUser(user);
    }


    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('STANDARD_USER')  or hasAuthority('MANAGER_USER') ")
    public User editUser(HttpServletRequest request, Principal principal, @RequestBody User user) {
        if (principal.getName().equals(user.getUsername())) {
            return userService.editUser(user);
        } else {
            throw new AccessDeniedException("User trying to modify unauthorized data !!!");
        }
    }
}
