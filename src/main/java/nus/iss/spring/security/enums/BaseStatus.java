package nus.iss.spring.security.enums;


public enum BaseStatus {
    ACTIVE, INACTIVE, DELETED
}
