package nus.iss.spring.security.service.impl;

import nus.iss.spring.security.domain.User;
import nus.iss.spring.security.repository.UserRepository;
import nus.iss.spring.security.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Random;


@Service
public class UserServiceImpl implements UserService {
    static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserRepository userRepository;


    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public User createUser(User user) {
        if (!StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        } else {
            Random random = new Random(100000);
            int randPassword = random.nextInt(999999);
            String password = String.format("%06d", randPassword);
            //TODO send email / sms
            logger.info("Auto generated password : Consider this as emailed\n--------------------------\n Password :" + password + "\n------------------------------------");
            user.setPassword(bCryptPasswordEncoder.encode(password));
        }
        return userRepository.save(user);
    }

    @Override
    public User editUser(User user) {
        User existingUser = userRepository.findByUsername(user.getUsername());
        existingUser.setFirstName(user.getFirstName());
        existingUser.setLastName(user.getLastName());
        return userRepository.save(existingUser);
    }
}
