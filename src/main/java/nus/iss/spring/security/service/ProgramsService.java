package nus.iss.spring.security.service;

import nus.iss.spring.security.domain.Program;

public interface ProgramsService {
    Iterable<Program> findAllPrograms();

    Program createProgram(Program program);

    Program editProgram(Program program);

    Program deleteProgram(Long programId);
}
