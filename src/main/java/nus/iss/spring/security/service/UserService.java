package nus.iss.spring.security.service;

import nus.iss.spring.security.domain.User;

import java.util.List;

public interface UserService {
    User findByUsername(String username);

    List<User> findAllUsers();

    User createUser(User user);

    User editUser(User user);
}
