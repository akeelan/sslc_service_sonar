package nus.iss.spring.security.service.impl;

import nus.iss.spring.security.domain.Program;
import nus.iss.spring.security.enums.BaseStatus;
import nus.iss.spring.security.exceptions.MatchNotFoundException;
import nus.iss.spring.security.repository.ProgramRepository;
import nus.iss.spring.security.service.ProgramsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProgramsServiceImpl implements ProgramsService {

    @Autowired
    private ProgramRepository programRepository;

    @Override
    public Iterable<Program> findAllPrograms() {
        return programRepository.findAll();
    }

    @Override
    public Program createProgram(Program program) {
        program.setStatus(BaseStatus.ACTIVE.name());
        return programRepository.save(program);
    }

    @Override
    public Program editProgram(Program program) {
        Optional<Program> optionalProgram = programRepository.findById(program.getId());
        if (optionalProgram.isPresent()) {
            Program program1 = optionalProgram.get();
            program1.setDescription(program.getDescription());
            program1.setName(program.getName());
            program = programRepository.save(program1);
        } else {
            throw new MatchNotFoundException(program.toString());
        }
        return program;
    }

    @Override
    public Program deleteProgram(Long id) {
        Optional<Program> optionalProgram = programRepository.findById(id);
        if (optionalProgram.isPresent()) {
            Program program1 = optionalProgram.get();
            program1.setStatus(BaseStatus.DELETED.name());
            return programRepository.save(program1);
        } else {
            throw new MatchNotFoundException(String.valueOf(id));
        }
    }
}
