package nus.iss.spring.security.repository;

import nus.iss.spring.security.domain.Program;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProgramRepository extends CrudRepository<Program, Long> {



}
