package com.prms.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MatchNotFoundException  extends RuntimeException{
    private static final String def = "Element match not found for : [%s]";

    public MatchNotFoundException(String message) {
        super(String.format(def, message));
    }

    public MatchNotFoundException(String message, Throwable cause) {
        super(String.format(def, message), cause);
    }
}
