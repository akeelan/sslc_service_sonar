package com.prms.service;

import com.prms.domain.Role;
import com.prms.domain.User;

import java.util.List;

public interface UserService {
    User findByUsername(String username);

    List<User> findAllUsers();

    User getUser(Long userID);

    User createUser(User user);

    User editUser(User user);

    void deleteUser(Long userID);

    List<Role> getAllRoles();
}
