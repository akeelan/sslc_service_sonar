package com.prms.service;

import com.prms.domain.Program;

import java.util.List;

public interface ProgramService {

    List<Program> findAllPrograms();

    Program getProgram(Long programID);

    Program createProgram(Program program);

    Program editProgram(Program program);

    Program deleteProgram(Long programID);

}
