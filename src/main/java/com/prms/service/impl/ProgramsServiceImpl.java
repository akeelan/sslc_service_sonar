package com.prms.service.impl;

import com.prms.domain.Program;
import com.prms.enums.BaseStatus;
import com.prms.exceptions.MatchNotFoundException;
import com.prms.repository.ProgramRepository;
import com.prms.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProgramsServiceImpl implements ProgramService {

    @Autowired
    private ProgramRepository programRepository;

    @Override
    public List<Program> findAllPrograms() {
        List<Program> programs = (List<Program>) programRepository.findAll();
        return programs.stream().filter(program -> (BaseStatus.ACTIVE.name().equalsIgnoreCase(program.getStatus()))).collect(Collectors.toList());
    }

    @Override
    public Program getProgram(Long programID) {
        Optional<Program> programOptional = programRepository.findById(programID);
        return programOptional.isPresent() ? programOptional.get() : null;
    }

    @Override
    public Program createProgram(Program program) {
        program.setStatus(BaseStatus.ACTIVE.name());
        return programRepository.save(program);
    }

    @Override
    public Program editProgram(Program program) {
        Optional<Program> optionalProgram = programRepository.findById(program.getId());
        if (optionalProgram.isPresent()) {
            Program program1 = optionalProgram.get();
            program1.setDescription(program.getDescription());
            program1.setName(program.getName());
            program = programRepository.save(program1);
        } else {
            throw new MatchNotFoundException(program.toString());
        }
        return program;
    }

    @Override
    public Program deleteProgram(Long id) {
        Optional<Program> optionalProgram = programRepository.findById(id);
        if (optionalProgram.isPresent()) {
            Program program1 = optionalProgram.get();
            program1.setStatus(BaseStatus.DELETED.name());
            return programRepository.save(program1);
        } else {
            throw new MatchNotFoundException(String.valueOf(id));
        }
    }
}
