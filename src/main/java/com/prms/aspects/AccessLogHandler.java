package com.prms.aspects;

import com.prms.domain.AccessLog;
import com.prms.repository.AccessLogRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;

@Aspect
@Configuration
public class AccessLogHandler {
    private static Logger logger = LoggerFactory.getLogger(AccessLogHandler.class);

    @Autowired
    private AccessLogRepository repository;

    @Pointcut("execution(* com.prms.controller.*.*(..))")
    public void controllers() {
    }

    @Around("controllers() && args(request, principal, ..)")
    public Object around(ProceedingJoinPoint joinPoint, HttpServletRequest request, Principal principal) throws Throwable {


        AccessLog accessLog = new AccessLog();

        long startTime = System.currentTimeMillis();

        accessLog.setAccessUser(principal.getName());

        accessLog.setAccessTime(LocalDateTime.now());

        accessLog.setMethodName(joinPoint.getSignature().getName());
        accessLog.setClassName(joinPoint.getTarget().getClass().getName());
        accessLog.setArguments(Arrays.toString(joinPoint.getArgs()));

        if (null != request) {
            accessLog.setHttpMethodType(request.getMethod());

            HashMap<String, String> headerValues = new HashMap<>();
            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                String headerValue = request.getHeader(headerName);
                headerValues.put(headerName, headerValue);
            }
            accessLog.setHeader(headerValues.toString());
            accessLog.setRequestPath(request.getServletPath());
        }

        Object result = joinPoint.proceed();

        long timeTaken = System.currentTimeMillis() - startTime;
        accessLog.setProcessTime(timeTaken);

        repository.save(accessLog);

        return result;

    }

}
