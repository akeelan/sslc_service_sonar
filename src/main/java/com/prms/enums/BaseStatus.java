package com.prms.enums;


public enum BaseStatus {
    ACTIVE, INACTIVE, DELETED
}
