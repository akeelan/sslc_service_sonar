package com.prms.controller;

import com.prms.domain.Program;
import com.prms.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/programs")
public class ProgramController {

    @Autowired
    private ProgramService programService;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Iterable<Program> getPrograms() {
        return programService.findAllPrograms();
    }

    @GetMapping(value = "/{programID}")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Program getUser(@PathVariable("programID") Long programID) {
        return programService.getProgram(programID);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Program createProgram(@Valid @RequestBody Program program) {
        return programService.createProgram(program);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Program editProgram(@Valid @RequestBody Program program) {
        return programService.editProgram(program);
    }

    @DeleteMapping(value = "/{programID}")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('MANAGER_USER') ")
    public Program deleteProgram(@PathVariable(name = "programID") Long programID) {
        return programService.deleteProgram(programID);
    }

}
