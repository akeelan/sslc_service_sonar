package com.prms.controller;

import com.prms.domain.Role;
import com.prms.domain.User;
import com.prms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('STANDARD_USER')  or hasAuthority('MANAGER_USER') ")
    public List<User> getUsers(HttpServletRequest request) {
        return userService.findAllUsers();
    }

    @GetMapping(value = "/{userID}")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('STANDARD_USER')  or hasAuthority('MANAGER_USER') ")
    public User getUser(@PathVariable("userID") Long userID) {
        return userService.getUser(userID);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public User createUser(@Valid @RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('STANDARD_USER')  or hasAuthority('MANAGER_USER') ")
    public User editUser(Principal principal, @RequestBody User user) {
        //if (principal.getName().equals(user.getUsername())) {
            return userService.editUser(user);
        //} else {
        //    throw new AccessDeniedException("User trying to modify unauthorized data !!!");
        //}
    }

    @DeleteMapping(value = "/{userID}")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public void deleteUser(@PathVariable("userID") Long userID) {
        userService.deleteUser(userID);
    }

    @GetMapping(value = "/roles")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('PRODUCER_USER') or hasAuthority('PRESENTER_USER') or hasAuthority('STANDARD_USER')  or hasAuthority('MANAGER_USER') ")
    public List<Role> getRoles(HttpServletRequest request) {
        return userService.getAllRoles();
    }
}
