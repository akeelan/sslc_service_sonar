package com.prms.repository;

import com.prms.domain.AccessLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessLogRepository   extends CrudRepository<AccessLog, Long> {
}
