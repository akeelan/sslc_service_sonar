package com.prms.domain;

import javax.persistence.*;
import java.security.Principal;
import java.time.LocalDateTime;

@Entity
@Table(name = "access_logs")
public class AccessLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime accessTime;
    private Long processTime;
    private String methodName;
    private String arguments;
    private String className;
    private String httpMethodType;
    private String header;
    private String requestPath;

    private String accessUser;

    @Override
    public String toString() {
        return "AccessLog{" +
                "id=" + id +
                ", accessTime=" + accessTime +
                ", processTime='" + processTime + '\'' +
                ", methodName='" + methodName + '\'' +
                ", arguments='" + arguments + '\'' +
                ", className='" + className + '\'' +
                ", httpMethodType='" + httpMethodType + '\'' +
                ", header='" + header + '\'' +
                ", requestPath='" + requestPath + '\'' +
                ", accessUser=" + accessUser +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(LocalDateTime accessTime) {
        this.accessTime = accessTime;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getArguments() {
        return arguments;
    }

    public void setArguments(String arguments) {
        this.arguments = arguments;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getHttpMethodType() {
        return httpMethodType;
    }

    public void setHttpMethodType(String httpMethodType) {
        this.httpMethodType = httpMethodType;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public String getAccessUser() {
        return accessUser;
    }

    public void setAccessUser(String accessUser) {
        this.accessUser = accessUser;
    }

    public Long getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Long processTime) {
        this.processTime = processTime;
    }
}
