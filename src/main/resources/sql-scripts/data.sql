INSERT INTO app_role (id, role_name, description) VALUES (1, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks');
INSERT INTO app_role (id, role_name, description) VALUES (2, 'MANAGER_USER', 'Manage User - Has no admin rights');
INSERT INTO app_role (id, role_name, description) VALUES (3, 'STANDARD_USER', 'Standard User - Has no admin rights');
INSERT INTO app_role (id, role_name, description) VALUES (4, 'PRESENTER_USER', 'Presenter User - Has no admin rights');
INSERT INTO app_role (id, role_name, description) VALUES (5, 'PRODUCER_USER', 'Producer User - Has no admin rights');

-- USER
-- non-encrypted password: jwtpass
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (1, 'Admin', 'Admin', '$2a$10$qtH0F1m488673KwgAfFXEOWxsoZSeHqqlB/8BTt3a6gsI5c2mdlfe', 'admin');
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (2, 'Manager', 'Manager', '$2a$10$qtH0F1m488673KwgAfFXEOWxsoZSeHqqlB/8BTt3a6gsI5c2mdlfe', 'manager');
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (3, 'Standard', 'Standard', '$2a$10$qtH0F1m488673KwgAfFXEOWxsoZSeHqqlB/8BTt3a6gsI5c2mdlfe', 'standard');
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (4, 'Presenter', 'Presenter', '$2a$10$qtH0F1m488673KwgAfFXEOWxsoZSeHqqlB/8BTt3a6gsI5c2mdlfe', 'presenter');
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (5, 'Producer', 'Producer', '$2a$10$qtH0F1m488673KwgAfFXEOWxsoZSeHqqlB/8BTt3a6gsI5c2mdlfe', 'producer');


INSERT INTO user_role(user_id, role_id) VALUES(1,1);
INSERT INTO user_role(user_id, role_id) VALUES(1,2);
INSERT INTO user_role(user_id, role_id) VALUES(1,3);
INSERT INTO user_role(user_id, role_id) VALUES(1,4);
INSERT INTO user_role(user_id, role_id) VALUES(1,5);
INSERT INTO user_role(user_id, role_id) VALUES(2,2);
INSERT INTO user_role(user_id, role_id) VALUES(2,3);
INSERT INTO user_role(user_id, role_id) VALUES(2,4);
INSERT INTO user_role(user_id, role_id) VALUES(2,5);
INSERT INTO user_role(user_id, role_id) VALUES(3,3);
INSERT INTO user_role(user_id, role_id) VALUES(4,4);
INSERT INTO user_role(user_id, role_id) VALUES(5,4);
INSERT INTO user_role(user_id, role_id) VALUES(5,5);